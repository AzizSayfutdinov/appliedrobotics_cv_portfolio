%% Calcualate Homography
clc
clear all

%% Read images from file
Im1 = imread('board_l.jpeg');
Im2 = imread('board_r.jpeg');
Im1 = imread('pc_l.jpeg');
Im2 = imread('pc_r.jpeg');
Im1 = rgb2gray(Im1);
Im2 = rgb2gray(Im2);

%% Detect and extract features from both images.

pts1  = detectSURFFeatures(Im1);
pts2 = detectSURFFeatures(Im2);
[features1,validPts1] = extractFeatures(Im1,pts1);
[features2,validPts2] = extractFeatures(Im2,pts2);

% Match features.
index_pairs = matchFeatures(features1,features2,'MatchThreshold',1);
pts1 = validPts1(index_pairs(:,1));
pts2 = validPts2(index_pairs(:,2));
figure;

%% Show Matched Features
showMatchedFeatures(Im1, Im2, pts1,pts2,'montage');
title('Matched points,including outliers');

%% Estimate H
[P,inlierPts1,inlierPts2] = estimateGeometricTransform(pts1, pts2,'Projective');
H = P.T';

%% Show Inliers
showMatchedFeatures(Im1, Im2, inlierPts1, inlierPts2,'montage');
title('Matched points without outliers (RANSAC)');

%% Test

%% select point in first image
imshow(Im1); hold on;
point1 = ginput(1);
plot(point1(1), point1(2),'og','LineWidth',3);
point1(3) = 1;
point1 = point1';

%% Calculate homogeneous coordinates of corresponding point
%  in second image by using H and point1
point2 = H*point1

%% Calculate inhomogeneous coordinates of point2
px = point2(1)/point2(3)
py = point2(2)/point2(3)

%% Plot point2
figure
imshow(Im2);
hold on;
plot(px,py,'or','LineWidth',3);